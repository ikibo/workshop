package org.spy.application.root;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface EventsChannels {

    String USERS_CHANNEL = "users";

    @Input(USERS_CHANNEL)
    SubscribableChannel usersChannel();

}
