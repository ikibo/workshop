package org.spy.application.root;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpyController {

    @PostMapping("/spy")
    public void doJob(@RequestBody Object obj) {
        System.out.println("Spied after this object:");
        System.out.println(obj);
    }

}
