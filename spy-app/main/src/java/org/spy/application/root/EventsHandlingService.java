package org.spy.application.root;

import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
public class EventsHandlingService {

    @StreamListener("users")
    public void receiveUserCreatedEvent(@Payload Object payLoad) {
        System.out.println(payLoad);
    }

}
