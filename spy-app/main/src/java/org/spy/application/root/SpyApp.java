package org.spy.application.root;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.stream.annotation.EnableBinding;

@SpringBootApplication
@EnableDiscoveryClient
@EnableBinding(EventsChannels.class)
public class SpyApp {

    public static void main(String[] args) {
        SpringApplication.run(SpyApp.class, args);
    }

}
