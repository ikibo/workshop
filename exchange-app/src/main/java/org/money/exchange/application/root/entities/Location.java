package org.money.exchange.application.root.entities;

import lombok.Data;

@Data
public class Location {

    private String city;

    private String area;
}
