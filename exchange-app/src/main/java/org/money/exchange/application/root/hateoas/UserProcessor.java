package org.money.exchange.application.root.hateoas;

import org.money.exchange.application.root.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

@Component
public class UserProcessor implements ResourceProcessor<Resource<User>> {

    @Autowired
    private RepositoryEntityLinks entityLinks;

    @Override
    public Resource<User> process(Resource<User> resource) {

        User user = resource.getContent();

        resource.getLinks().clear();
        return resource;
    }

}
