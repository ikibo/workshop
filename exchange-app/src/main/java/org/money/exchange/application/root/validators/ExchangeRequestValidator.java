package org.money.exchange.application.root.validators;

import org.money.exchange.application.root.entities.ExchangeRequest;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class ExchangeRequestValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return ExchangeRequest.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {

        ExchangeRequest request = (ExchangeRequest) target;

        if (request.getAmount() < 0) {

        }

        if (request.getRate() < 0) {

        }

    }

}
