package org.money.exchange.application.root.repositories;

import org.money.exchange.application.root.entities.User;
import org.money.exchange.application.root.hateoas.UserBaseInfoProjection;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(excerptProjection = UserBaseInfoProjection.class)
public interface UserRepository extends PagingAndSortingRepository<User, String> {

    @Override
    // @RestResource(exported = false)
    <S extends User> S save(S user);

    @Override
    // @RestResource(exported = false)
    <S extends User> List<S> save(Iterable<S> entities);

    // @RestResource(path = "byPhone")
    List<User> findByActiveIsTrueAndPhoneNumber(@Param("phoneNumber") String phoneNumber);
}
