package org.money.exchange.application.root.services;

import org.money.exchange.application.root.entities.User;
import org.money.exchange.application.root.exceptions.UserRegistrationException;
import org.money.exchange.application.root.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static org.springframework.util.StringUtils.isEmpty;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    // TODO : what if smb tries to register user with login that already exists
    public User register (User user) {

        if (isEmpty(user.getLogin())) {
            throw new UserRegistrationException("Login may not be empty");
        }

        if (isEmpty(user.getPhoneNumber())) {
            throw new UserRegistrationException("Phone number may not empty");
        }

        if (isEmpty(user.getPassword())) {
            throw new UserRegistrationException("Password may not be empty");
        }

        user.setAdmin(false);
        user.setActive(true);

        if (isEmpty(user.getGivenName())) {
            user.setGivenName("none");
        }

        if (isEmpty(user.getFamilyName())) {
            user.setGivenName("none");
        }

        return userRepository.save(user);
    }

//    @Order(2)
//    @EventListener()
//    public void onBeforeConvert(BeforeConvertEvent<User> event) {
//        if (!User.class.isInstance(event.getSource())) {
//            return;
//        }
//
//        User user = event.getSource();
//        System.out.println("1" + user);
//    }
//
//
//    @Order(1)
//    @EventListener()
//    public void onBeforeConvert_(BeforeConvertEvent<User> event) {
//
//        if (!User.class.isInstance(event.getSource())) {
//            return;
//        }
//
//        User user = event.getSource();
//        System.out.println("2" + user);
//    }

}
