package org.money.exchange.application.root.security;

import org.money.exchange.application.root.entities.User;
import org.money.exchange.application.root.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class AppUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = userRepository.findOne(login);
        if (user == null) {
            throw new UsernameNotFoundException("User does not exist: " + login);
        }

        return new AppUser(user);
    }
}
