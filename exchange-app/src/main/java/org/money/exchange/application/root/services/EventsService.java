package org.money.exchange.application.root.services;

import org.money.exchange.application.root.entities.ExchangeRequest;
import org.money.exchange.application.root.entities.User;
import org.money.exchange.application.root.integration.SpyEventsProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.data.rest.core.annotation.HandleAfterSave;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Service;

@Service
@Order(0)
@RepositoryEventHandler
public class EventsService {

    @Autowired
    private SpyEventsProducer spyEventsProducer;

    @HandleBeforeCreate
    public void handleUserBeforeCreate(User user) {
        spyEventsProducer.sendCreationEvent(user);
    }

    @HandleBeforeCreate
    public void handleExchangeRequestBeforeCreate(ExchangeRequest request) {
        spyEventsProducer.sendCreationEvent(request);
    }

    @HandleAfterSave
    public void handleUserAfterSave(User user) {
        spyEventsProducer.sendCreationEvent(user);
    }


    @HandleBeforeSave
    public void handleUserBeforeSave(User user) {
        spyEventsProducer.sendCreationEvent(user);
    }

}
