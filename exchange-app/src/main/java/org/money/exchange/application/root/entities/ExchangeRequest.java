package org.money.exchange.application.root.entities;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

@Data
public class ExchangeRequest  {

    @Id
    private String id;

    private Type type;

    private Status status = Status.NEW;

    private Currency currency;

    private int amount;

    private int rate;

    private Location location;

    private String comment;

    @DBRef
    private User owner;

    @DBRef
    private User dealer;

    public enum Status {

        NEW, PUBLISHED, FULFILLED, CANCELLED

    }

    public enum Type {

        BID, ASK

    }

    public String toDecFormat() {
        String s = Integer.toString(amount);

        String first = s.substring(0, s.length() - 2);
        String second = s.substring(s.length() - 2, s.length());
        return first + "." + second;
    }

}
