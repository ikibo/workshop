package org.money.exchange.application.root.hateoas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

@Component
public class UserBaseInfoProcessor implements ResourceProcessor<Resource<UserBaseInfoProjection>> {

    @Autowired
    private RepositoryEntityLinks entityLinks;

    @Override
    public Resource<UserBaseInfoProjection> process(Resource<UserBaseInfoProjection> resource) {

        UserBaseInfoProjection user = resource.getContent();

        resource.getLinks().clear();
        return resource;
    }

}
