package org.money.exchange.application.root.integration;

import org.springframework.integration.annotation.Publisher;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
public class SpyEventsProducer {

   @Publisher(channel = SpyCommunicationChannels.USERS_CHANNEL)
   public Message<String> sendCreationEvent(Object anything) {
       return MessageBuilder.withPayload(anything.toString()).build();
   }
}
