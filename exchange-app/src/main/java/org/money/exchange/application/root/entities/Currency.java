package org.money.exchange.application.root.entities;

public enum Currency {
    USD, UAH, EUR, GBP, CHF
}