package org.money.exchange.application.root.security;

public enum Role {

   ANONYMOUS, USER, ADMIN

}
