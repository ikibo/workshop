package org.money.exchange.application.helloworld;

import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

import static java.lang.System.out;

@Service
public class HelloWorldService {

    private final static Function<String, String> PROCESS = s -> s + " processed";

    public void processInfo(Map<String, String> info) {
        info.values().stream()
            .filter(Objects::nonNull)
            .map(PROCESS)
            .reduce((info1, info2) -> info1 + "::" + info2)
            .ifPresent(out::println);
    }
}
