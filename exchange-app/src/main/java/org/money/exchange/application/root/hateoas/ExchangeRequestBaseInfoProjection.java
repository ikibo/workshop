package org.money.exchange.application.root.hateoas;

import org.money.exchange.application.root.entities.Currency;
import org.money.exchange.application.root.entities.ExchangeRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "baseInfo", types = ExchangeRequest.class)
public interface ExchangeRequestBaseInfoProjection {

    String getId();

    ExchangeRequest.Type getType();

    ExchangeRequest.Status getStatus();

    Currency getCurrency();

    @Value("#{ target.toDecFormat() }")
    String getAmount();


}
