package org.money.exchange.application.root.security;

import com.google.common.collect.ImmutableList;
import org.money.exchange.application.root.entities.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

import static org.money.exchange.application.root.security.Role.ADMIN;
import static org.money.exchange.application.root.security.Role.USER;

public class AppUser implements UserDetails {

    private final SimpleGrantedAuthority ROLE_USER = new SimpleGrantedAuthority(USER.name());
    private final SimpleGrantedAuthority ROLE_ADMIN = new SimpleGrantedAuthority(ADMIN.name());

    private final Collection<GrantedAuthority> USER_ROLES = ImmutableList.of(ROLE_USER);
    private final Collection<GrantedAuthority> ADMIN_ROLES = ImmutableList.of(ROLE_USER, ROLE_ADMIN);

    private final User user;

    public AppUser(User user) {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return user.isAdmin() ? ADMIN_ROLES : USER_ROLES;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getLogin();
    }

    @Override
    public boolean isAccountNonExpired() {
        return user.isActive();
    }

    @Override
    public boolean isAccountNonLocked() {
        return user.isActive();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return user.isActive();
    }

    @Override
    public boolean isEnabled() {
        return user.isActive();
    }
}
