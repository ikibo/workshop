package org.money.exchange.application.root.validators;

import org.money.exchange.application.root.entities.User;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import static org.apache.commons.lang3.StringUtils.isBlank;

public class UserValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        User user = (User) target;

        if (isBlank(user.getLogin())) {
            errors.rejectValue("login", "empty.login");
        }

        if (isBlank(user.getPassword())) {
            errors.rejectValue("password", "empty.password");
        }

        if (isBlank(user.getPhoneNumber())) {
            errors.rejectValue("phoneNumber", "empty.phoneNumber");
        }
    }
}
