package org.money.exchange.application.root.services;

import org.money.exchange.application.root.entities.ExchangeRequest;
import org.money.exchange.application.root.repositories.ExchangeRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExchangeRequestService {

    @Autowired
    private ExchangeRequestRepository exchangeRequestRepository;

    public ExchangeRequest publish(String id) {
        ExchangeRequest request = exchangeRequestRepository.findOne(id);
        request.setStatus(ExchangeRequest.Status.PUBLISHED);
        request = exchangeRequestRepository.save(request);
        return request;
    }

    public ExchangeRequest fullFill(ExchangeRequest exchangeRequest) {
        return null;
    }

    public ExchangeRequest cancel(ExchangeRequest exchangeRequest) {
        return null;
    }

    public ExchangeRequest expire(String id) {
        return null;
    }
}
