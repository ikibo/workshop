package org.money.exchange.application.root.hateoas;

import org.money.exchange.application.root.entities.User;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "fullInfo", types = User.class)
public interface UserFullInfoProjection {

    String getLogin();

    String getPhoneNumber();

    String getGivenName();

    String getFamilyName();

    boolean isActive();

    boolean isAdmin();

}
