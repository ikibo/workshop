package org.money.exchange.application.root.hateoas;

import org.money.exchange.application.root.entities.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "baseInfo", types = User.class)
public interface UserBaseInfoProjection {

    String getPhoneNumber();

    String getLogin();

    @Value("#{target.givenName + \", \" + target.familyName}")
    String getFullName();

}
