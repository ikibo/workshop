package org.money.exchange.application.root.integration;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface SpyCommunicationChannels {

    String USERS_CHANNEL = "users";

    String EXCHANGE_REQUEST_CHANNEL = "exchangeRequests";

    @Output(USERS_CHANNEL)
    MessageChannel userChannel();

    @Output(EXCHANGE_REQUEST_CHANNEL)
    MessageChannel exchangeRequestChannel();

}
