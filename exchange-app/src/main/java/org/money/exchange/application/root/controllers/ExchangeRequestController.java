package org.money.exchange.application.root.controllers;

import org.money.exchange.application.root.services.ExchangeRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@RepositoryRestController
public class ExchangeRequestController {

    @Autowired
    private ExchangeRequestService exchangeRequestService;

    @ResponseBody
    @PostMapping(value = "/requests/{id}/publish")
    public PersistentEntityResource publish(@PathVariable("id") String id,
                                            PersistentEntityResourceAssembler assembler) {

        return assembler.toFullResource(exchangeRequestService.publish(id));
    }

    @ResponseBody
    @PostMapping(value = "/requests/{id}/expire")
    public PersistentEntityResource expire(@PathVariable("id") String id,
                                            PersistentEntityResourceAssembler assembler) {

        return assembler.toFullResource(exchangeRequestService.expire(id));
    }

}
