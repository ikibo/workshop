package org.money.exchange.application.root.repositories;

import org.money.exchange.application.root.entities.ExchangeRequest;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "requests")
public interface ExchangeRequestRepository extends PagingAndSortingRepository<ExchangeRequest, String> {

}
