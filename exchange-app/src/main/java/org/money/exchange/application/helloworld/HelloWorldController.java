package org.money.exchange.application.helloworld;

import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class HelloWorldController {

    @Autowired
    private HelloWorldService helloWorldService;

    //@Autowired
    //private ExchangeApp.SpyClient spyClient;

    @GetMapping(value = "/get/{anyString}")
    public Map<String, String> getAnyString(@PathVariable("anyString") String x) {
        //spyClient.hello(ImmutableMap.of("anyString", anyString));
        return ImmutableMap.of("anyString", x);
    }

    @PostMapping("/post/{anyString}")
    public Map<String, String> helloWorld(@PathVariable String anyString) {
        return ImmutableMap.of("anyString", anyString);
    }

    @GetMapping("/info/{name}/{familyName}")
    public Map<String, String> getInfo(@PathVariable(value = "name") String name,
                                       @PathVariable(value = "familyName") String familyName,
                                       @RequestParam(value = "city", required = false) String city) {

        ImmutableMap<String, String> info = ImmutableMap.of(
            "name", parseText(name),
            "familyName", parseText(familyName),
            "city", parseText(city)
        );

        helloWorldService.processInfo(info);

        return info;
    }

    private static String parseText(String text) {
        if (StringUtils.isEmpty(text)) {
            return "none";
        }
        return text;
    }

}
