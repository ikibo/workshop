package org.money.exchange.application.root.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AppUserDetailsService userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

//    @Override
//    public void configure(WebSecurity web) {
//        web.ignoring().antMatchers(POST, "/users/register");
//    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http
            .httpBasic()

            .and()
            .authorizeRequests()
            .antMatchers("/**")
            .permitAll();

//            .and()
//            .authorizeRequests()
//            .antMatchers(POST, "/users/register")
//            .permitAll()
//
//            .and()
//            .authorizeRequests()
//            .antMatchers("/users/**")
//            .hasAuthority(Role.ADMIN.name())
//
//            .and()
//            .authorizeRequests()
//            .antMatchers("/**")
//            .authenticated();
    }

}
