package org.money.exchange.application.root.hateoas;

import org.money.exchange.application.root.controllers.ExchangeRequestController;
import org.money.exchange.application.root.entities.ExchangeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class ExchangeRequestProcessor implements ResourceProcessor<Resource<ExchangeRequest>> {

    @Autowired
    private RepositoryEntityLinks entityLinks;

    @Override
    public Resource<ExchangeRequest> process(Resource<ExchangeRequest> resource) {

        ExchangeRequest request = resource.getContent();

        resource.add(entityLinks.linkToSingleResource(ExchangeRequest.class, request.getId()).withRel("delete"));

        resource.add(entityLinks.linkForSingleResource(ExchangeRequest.class, request.getId()).slash("expire").withRel("expire"));

        resource.add(linkTo(methodOn(ExchangeRequestController.class).publish(request.getId(), null)).withRel("publish"));

        return resource;
    }

}
