package org.money.exchange.application.root.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Id
    // @NotBlank(message = "Login may not be empty")
    private String login;

    // @NotBlank(message = "Phone number may not be empty")
    private String phoneNumber;

    // @NotBlank(message = "Password may not be empty")
    private String password;

    private String givenName;

    private String familyName;

    @JsonIgnore
    private boolean active;

    @JsonIgnore
    private boolean admin;
}
