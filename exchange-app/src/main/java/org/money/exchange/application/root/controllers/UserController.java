package org.money.exchange.application.root.controllers;

import org.money.exchange.application.root.entities.User;
import org.money.exchange.application.root.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/users/register")
    @ResponseStatus(HttpStatus.CREATED)
    public User register(@RequestBody User  user) {
        return userService.register(user);
    }

}
