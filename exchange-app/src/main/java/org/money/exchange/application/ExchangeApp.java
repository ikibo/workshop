package org.money.exchange.application;

import org.money.exchange.application.root.entities.ExchangeRequest;
import org.money.exchange.application.root.entities.User;
import org.money.exchange.application.root.hateoas.UserBaseInfoProjection;
import org.money.exchange.application.root.hateoas.UserFullInfoProjection;
import org.money.exchange.application.root.integration.SpyCommunicationChannels;
import org.money.exchange.application.root.repositories.UserRepository;
import org.money.exchange.application.root.validators.UserValidator;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@EnableBinding(SpyCommunicationChannels.class)
public class ExchangeApp {

    public static void main(String[] args) {
        SpringApplication.run(ExchangeApp.class, args);
    }

    @Configuration
    public static class AppRepositoryRestMvcConfiguration extends RepositoryRestConfigurerAdapter {

        @Override
        public void configureValidatingRepositoryEventListener(ValidatingRepositoryEventListener validatingListener) {
            validatingListener.addValidator("beforeCreate", new UserValidator());
        }

        @Override
        public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
            config.exposeIdsFor(User.class, ExchangeRequest.class);
            config.getProjectionConfiguration().addProjection(UserBaseInfoProjection.class);
            config.getProjectionConfiguration().addProjection(UserFullInfoProjection.class);
        }
    }

    @FeignClient("spy")
    public interface SpyClient {

        @RequestMapping(value = "/spy", method = POST)
        String hello(Object body);

    }

    @Bean
    CommandLineRunner commandLineRunner(UserRepository userRepository) {
        return ($) -> {
            User admin =
                User.builder()
                    .login("admin")
                    .familyName("admin")
                    .givenName("admin")
                    .active(true)
                    .admin(true)
                    .password("admin")
                    .build();

            userRepository.save(admin);
        };
    }

}
